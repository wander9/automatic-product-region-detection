import pathlib
import numpy as np
from numpy import min_scalar_type
import torch
from pytorch_faster_rcnn_tutorial.utils import get_filenames_of_path
from pytorch_faster_rcnn_tutorial.datasets import ObjectDetectionDataSet
from pytorch_faster_rcnn_tutorial.visual import DatasetViewer
from pytorch_faster_rcnn_tutorial.transformations import ComposeDouble, Clip, FunctionWrapperDouble, normalize_01
import io
import json
import os

# Import the data
root = pathlib.Path('pytorch_faster_rcnn_tutorial/data/heads')
#find . -name ".DS_Store" -delete

# Specify the annotations as targets
targets = get_filenames_of_path(root / 'target')
inputs = get_filenames_of_path(root / 'input')
targets.sort()

#load data
t_0 = targets[0]
t_0_f = open(t_0)
t_0_data = json.load(t_0_f)
annotation = t_0_data

#overview of keys, labels & boxes from single annotation file
print("example annotation file:")
print(f'keys:\n{annotation.keys()}\n')
print(f'labels:\n{annotation["labels"]}\n')
print(f'boxes:\n{annotation["boxes"]}\n') 
print("number of regions: " + str(len(annotation["labels"])))


#accumalate summary statistics
number_regions = 0
max_number_regions = 0
min_number_regions = 1000

directory = os.getcwd() + "/pytorch_faster_rcnn_tutorial/data/shelves/target"
for i in range(len(targets)):
    t = targets[i]
    t_f = open(t)
    t_data = json.load(t_f)
    annotation = t_data
    number_regions += len(annotation["labels"])
    if len(annotation["labels"]) > max_number_regions:
        max_number_regions = len(annotation["labels"])
    if len(annotation["labels"]) < min_number_regions:
        min_number_regions = len(annotation["labels"])

#count number of images, total number of regions, average number of regions per picture, max #regions per picture, min #regions per picture
print("some summary statistics:")
print("number of images: " + str(len(targets)))
print("total number of regions: " + str(number_regions))
print("avg number of regions: " + str(number_regions/len(targets)))
print("max number of regions: " + str(max_number_regions))
print("min number of regions: " + str(min_number_regions))

