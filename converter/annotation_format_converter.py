import json
import os
from annotation_model import BoxAnnotation


def convert_annotations(source_file):
    print("Converting format of annotations...")

    # Open JSON file, load the data, and then close the file
    f = open(source_file)
    data = json.load(f)
    f.close()

    # Create annotations
    annotations = data[0]['annotations']
    box_annotations = []
    boxes = []
    labels = []
    for annotation in  annotations:
        box = BoxAnnotation.init_from_source_format(annotation['coordinates'])
        box_annotations.append(box)

        # Required for JSON file output
        boxes.append(box.get_coordinates())
        labels.append(box.label)

    # Save annotations in JSON format
    new_annotations = {
        'labels': labels,
        'boxes': boxes
    }
    json_string = json.dumps(new_annotations)
    file_path = os.getcwd() + "/converter/targets_new/" + file
    json_file = open(file_path, "w")
    json_file.write(json_string)
    json_file.close()

    print("Annotations successfully converted.")
    print(f"File successfully saved: {file_path}")


if __name__ == "__main__":

    directory = os.getcwd() + "/converter/targets_source"
    print(os.listdir(directory))
    for file in os.listdir(directory):
    
        source_file = os.getcwd() + "/converter/targets_source/" + file
        convert_annotations(source_file=source_file)