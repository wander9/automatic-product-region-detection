
# Annotation class

class BoxAnnotation():

    def __init__(self, label, x_top_left, y_top_left, x_bottom_right, y_bottom_right):
        """Model of an box that can annotate product regions

        :param label: Description of the annotation
        :param x_top_left: x coordinate of top left corner
        :param y_top_left: y coordinate of top left corner
        :param x_bottom_right: x coordinate of bottom right corner
        :param y_bottom_right: y coordinate of bottom right corner

        :return: Box annotation 

        """

        self.label = label
        self.x_top_left = x_top_left
        self.y_top_left = y_top_left
        self.x_bottom_right = x_bottom_right
        self.y_bottom_right = y_bottom_right

    
    @classmethod
    def init_from_source_format(cls, coords: dict) -> "BoxAnnotation":
        
        # coords = [ann['coordinates'] for ann in source['annotations']]

        x_top_left = coords['x'] - 0.5*coords['width']
        y_top_left = coords['y'] - 0.5*coords['height']

        x_bottom_right = coords['x'] + 0.5*coords['width']
        y_bottom_right = coords['y'] + 0.5*coords['height']

        box = cls("region", x_top_left, y_top_left, x_bottom_right, y_bottom_right)

        if y_bottom_right <= y_top_left:
            print("WHYYYYYYYYYYY")
        if x_bottom_right <= x_top_left:
            print("WHXXXXXXXXXXX")
        if coords['width'] <= 0:
            print("W0")
        if coords['height'] <= 0:
            print("H0")

        return box

    
    def get_coordinates(self):
        return self.x_top_left, self.y_top_left, self.x_bottom_right, self.y_bottom_right