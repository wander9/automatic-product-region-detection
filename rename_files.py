import pathlib

from pytorch_faster_rcnn_tutorial.utils import get_filenames_of_path

root = pathlib.Path('pytorch_faster_rcnn_tutorial/data/shelves')

inputs = get_filenames_of_path(root / 'input')
inputs.sort()
#find . -name ".DS_Store" -delete

for idx, path in enumerate(inputs):
    old_name = path.stem
    old_extension = path.suffix
    dir = path.parent
    new_name = str(idx).zfill(3) + old_extension
    print(f'old: {old_name + old_extension}')
    print(f'new: {new_name}')
    path.rename(pathlib.Path(dir, new_name))

outputs = get_filenames_of_path(root / 'target')
outputs.sort()
#find . -name ".DS_Store" -delete

for idx, path in enumerate(outputs):
    old_name = path.stem
    old_extension = path.suffix
    dir = path.parent
    new_name = str(idx).zfill(3) + old_extension
    print(f'old: {old_name + old_extension}')
    print(f'new: {new_name}')
    path.rename(pathlib.Path(dir, new_name))